from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
feed_item = Table('feed_item', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('feed_id', Integer),
    Column('title', String(length=255)),
    Column('link', String(length=255)),
    Column('description', Text),
    Column('image', String(length=255)),
    Column('pubDate', String(length=255)),
    Column('duration', String(length=255)),
    Column('enclosure', Text),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['feed_item'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['feed_item'].drop()
