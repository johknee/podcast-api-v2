from app import app, db
from flask import jsonify, render_template, request, g, session, make_response
from models import User, Feed
from decorators import login_required
from auth import oauth_check
import datetime
from handlers import rssparse, itemsaver


def return_user(gResp):
	if gResp is not None:
		q = User.query.filter(User.email == gResp['email'])
		if q.count():
			u = q.first()
		else:
			u = User(google_id=gResp['sub'], email=gResp['email'], name=gResp['name'])
			db.session.add(u)
			db.session.commit()

		return u
		
	return None

@app.before_request
def check_login():
	g.user = None
	if not hasattr(g, 'skip'):
		if request.authorization:
			gResp = oauth_check(request.authorization.username)
			g.user = return_user(gResp)

@app.route('/')
def index():
	return make_response(jsonify({
		"foo": "bar"
	}), 200)

@app.route('/loginform', methods=['GET'])
def loginform():
	return render_template('login.html')

@app.route('/login', methods=['GET'])
def login():
	return make_response(jsonify({
		"error": "true",
		"message": "Login required"
	}), 403)

@app.route('/savetoken', methods=['POST'])
def loginpost():
	gResp = oauth_check(request.form['token'])

	u = return_user(gResp)

	resp = make_response(jsonify({
		"user": u.serialize()
	}), 200)
	return resp

@app.route('/feed', methods=['POST'])
@login_required
def post_feed():
	url = request.form['url']
	q = Feed.query.filter(Feed.link == url)

	status = 200

	if q.count():
		feed = q.first()
	else:
		r = rssparse.request_url(url=url)

		if r['result'] == 'error':
			status = 400
		else:
			meta = r['data']['meta']
			feed = Feed(title=meta['title'], link=url, data=meta)
			itemsaver.save_items(feed, r['data']['items'])
			g.user.feeds.append(feed)
			db.session.add(g.user)
			db.session.commit()

	return make_response(jsonify(feed.serialize(detail=True)), status)

