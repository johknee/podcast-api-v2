from app import db
from ..models import FeedItem

def save_items(feed, items):
	for item in items:
		q = FeedItem.query.filter(FeedItem.link == item['link'])

		if q.count() == 0:
			feeditem= FeedItem(
				title=item['title'],
				link=item['link'],
				enclosure=item['enclosure'],
				image=item['image'],
				description=item['description'],
				pubDate=item['pubDate'],
				duration=item['duration']
			)
			feed.items.append(feeditem)

	db.session.add(feed)
	db.session.commit()
