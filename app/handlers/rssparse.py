import json, sys, urlparse
import traceback
import requests
import xmltodict

def request_url(url=None):
    if url is not None:
        try:
            r = requests.get(url)

            if r.status_code == 200:
                _raw = r.content
                _tree = xmltodict.parse(_raw)['rss']
                data = {};

                if '@xmlns:atom' in _tree:
                    _type = 'atom'
                    data['meta'] = _get_meta(_tree['channel'])
                    data['items'] = _get_items(_tree['channel'])

                return _respond('success', data)
            else:
                return _error('Error grabbing url')
        except:
            print traceback.print_exc()
            return _error('Error grabbing url')
    else:
        return _error('No url')

def _get_meta(data):
    meta = {}

    for k,v in data.items():
        if k != 'item':
            meta[k] = v

    normalised_meta = _normalise_meta(meta)

    return normalised_meta

def _normalise_meta(meta):
    normalised_meta = {}
    normalised_meta['title'] = _cycle_keys(meta, ['title'])
    normalised_meta['link'] = _cycle_keys(meta, ['link'])
    normalised_meta['description'] = _cycle_keys(meta, ['description'])
    normalised_meta['image'] = _cycle_keys(meta, ['image'])
    normalised_meta['pubDate'] = _cycle_keys(meta, ['pubDate'])
    return normalised_meta

def _cycle_keys(data, keys):
    for v in keys:
        if v in data:
            return data[v]

    return False

def _get_items(data):
    if 'item' in data:
        return _normalise_items(data['item'])
    else:
        return []

def _normalise_items(items):
    normalised_items = []

    for item in items:
        n_item = {}
        n_item['title'] = _cycle_keys(item, ['title'])
        n_item['link'] = _cycle_keys(item, ['link'])
        n_item['description'] = _cycle_keys(item, ['description', 'itunes:summary'])
        n_item['image'] = _get_image(_cycle_keys(item, ['image', 'itunes:image']))
        n_item['pubDate'] = _cycle_keys(item, ['pubDate'])
        n_item['duration'] = _cycle_keys(item, ['itunes:duration'])
        n_item['enclosure'] = _cycle_keys(item, ['enclosure'])
        normalised_items.append(n_item)

    return normalised_items

def _get_image(image):
    img = 'default'

    if isinstance(image, basestring):
        if _valid_url(image):
            img = image
    elif '@href' in image:
    	img = image['@href']

    return img

def _valid_url(url):
	parsed_url = urlparse.urlparse(url)
	return bool(parsed_url.scheme)


def _error(msg):
    return _respond('error', {
            'message': msg
        })

def _respond(result, data):
    return {
        'result': result,
        'data': data
    }
