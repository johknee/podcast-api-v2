from functools import wraps
from flask import g, request, redirect, url_for
from auth import oauth_check

def check_auth(token):
    return oauth_check(token)

def login_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if g.user is None:
            g.skip = True;
            return redirect(url_for('login', next=request.url))
        return f(*args, **kwargs)
    return decorated

