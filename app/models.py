from app import db
from sqlalchemy.orm import synonym
import json

user_feeds = db.Table('user_feeds', db.Model.metadata,
	db.Column('id', db.Integer, primary_key=True),
	db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
	db.Column('feed_id', db.Integer, db.ForeignKey('feed.id'))
)

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	google_id = db.Column(db.String(255), index=True, unique=True)
	email = db.Column(db.String(120), index=True, unique=True)
	name = db.Column(db.String(255))
	feeds = db.relationship("Feed",
					secondary=user_feeds)

	def __repr__(self):
		return '<User %r>' % (self.name)

	def serialize(self, detail=False):
		obj = {
			"id": self.id,
			"google_id": self.google_id,
			"email": self.email,
			"name": self.name,
		}

		if detail:
			obj['feeds'] = [i.serialize() for i in self.feeds]

		return obj

class Feed(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(120))
	link = db.Column(db.String(255), index=True, unique=True)
	items = db.relationship("FeedItem", back_populates="feed")
	_data = db.Column('data', db.Text)

	@property
	def data(self):
		return json.loads(self._data);

	@data.setter
	def data(self, value):
		self._data = json.dumps(value)

	data = synonym('_data', descriptor=data)

	def serialize(self, detail=False):
		obj = {
			"id": self.id,
			"title": self.title,
			"link": self.link,
			"data": self.data
		}

		if detail:
			obj['items'] = [i.serialize() for i in self.items]

		return obj

l = []
for i in range(10):
	if i % 2:
		l.append(i)


l = [i for i in range(10) if i%2]


class FeedItem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    feed_id = db.Column(db.Integer, db.ForeignKey('feed.id'))
    feed = db.relationship("Feed", back_populates="items")
    title = db.Column(db.String(255), index=True)
    link = db.Column(db.String(255))
    description = db.Column(db.Text)
    image = db.Column(db.String(255))
    pubDate = db.Column(db.String(255), index=True)
    duration = db.Column(db.String(255), index=True)
    _enclosure = db.Column('enclosure', db.Text)

    @property
    def enclosure(self):
        return json.loads(self._enclosure);

    @enclosure.setter
    def enclosure(self, value):
        self._enclosure = json.dumps(value)

    enclosure = synonym('_enclosure', descriptor=enclosure)

    def serialize(self, detail=False):
    	obj = {
    		"id": self.id,
    		"title": self.title,
    		"link": self.link,
    		"description": self.description,
    		"image": self.image,
    		"pubDate": self.pubDate,
    		"duration": self.duration,
    		"enclosure": self.enclosure
    	}

    	if detail:
    		obj['feed'] = self.feed

    	return obj
