from oauth2client import client, crypt
import logging
from config import WEB_CLIENT_ID, CLIENT_ID

def oauth_check(token):
  try:
      idinfo = client.verify_id_token(token, CLIENT_ID)
      # If multiple clients access the backend server:
      if idinfo['aud'] not in [WEB_CLIENT_ID]:
          raise crypt.AppIdentityError("Unrecognized client.")
      if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
          raise crypt.AppIdentityError("Wrong issuer.")
      # if idinfo['hd'] != APPS_DOMAIN_NAME:
      #     raise crypt.AppIdentityError("Wrong hosted domain.")
  except crypt.AppIdentityError:
      return None
      
  return idinfo

