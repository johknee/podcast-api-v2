import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'podcast.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
WEB_CLIENT_ID = '994024216115-6jk9cme5d3hbnmlli4tcs1nbpcrnj983.apps.googleusercontent.com'
CLIENT_ID = '994024216115-6jk9cme5d3hbnmlli4tcs1nbpcrnj983.apps.googleusercontent.com'
SQLALCHEMY_TRACK_MODIFICATIONS = False